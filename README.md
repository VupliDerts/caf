# CAF - Collect All Files

## About
This is a fork of the very useful Collect all Files addon for Blender, originally [here](https://github.com/Begalov/CAF-collect-all-file-addon).  The purpose of this addon is to gather external files linked into the blend file and put them all in a single folder so that they can be easily transported.

### Improvements
- Asset folder includes name of file it is derived from.
- Error handling for different data types.

## Changelog

### version: (0, 5)
Error handling for fonts, external libraries, movie clips, image sequences, and sounds has been improved. The scope of the addon has been limited to collecting all files of all types + images - pending further review - to improve stability and UX.

### version: (0, 4)
Minor improvements including changing the collected files directory name to include the name of the .blend file so it can be distinguished from others.  I have also improved image collection.  Images without data are now ignored, and a message is written to the log.

## Installation
Download the .zip file. In Blender go to Edit > Preferences, navigate to the 'Addons' tab and install the zip file.  There is no need to unzip the addon.

## Usage
In order to use this addon you .blend file must be saved.  To collect all files go to File > External Data > Collect all Files.  This will create a folder next to your blend file which contains all of the externally linked assets gathered.
